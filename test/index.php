<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <title>Mathematik-Nachhilfe</title>
        
        <link rel="stylesheet" href="css/styles.css" media="screen" />
        <link rel="stylesheet" href="css/other.css" />
        <link rel="shortcut icon" href="img/logo.png" />

        <!-- JS -->
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script src="js/funktionen_andy.js"></script>
    </head>
    <body>
        <div class="overlayer"></div>
        <header class="hauptmenue">
            <div id="queickmenuedialogplaceholder" class="cl_quick_placeholder"></div>
            <img  src="img/sandwich.png" class="sandwitchmenue"></img>
            <div class="headertitel"><a href="index.php">Mathematik Nachhilfe</a></div>

            <div class="menuenormal">
                <div id="menueStart" class="hauptmenuepunkt menueStart class_dunkel">Start</div>
                <div id="menueSchueler" class="menueSchueler hauptmenuepunkt" >Für Schüler</div>
                <div id="menueEltern" class="menueEltern hauptmenuepunkt" >Für Eltern</div>
                <div id="menueUebermich" class="menueUebermich hauptmenuepunkt">Über mich</div>
                <div id="menueKontakt" class="menueKontakt hauptmenuepunkt" >Kontakt</div>
            </div>
        </header>

        <main id="reiter">

        </main>

        <footer>
            <div class="oberfooter">
                <div class="oberfooterInhalt">
                    <div style="float: left; width: 200px; margin-right: 90px; margin-bottom: 20px;">
                        <p style="font-weight: bold;">KONTAKT</p>
                        <p>Hr. Andy Shamoon</p>
                        <p>Antonsplatz 18</p>
                        <p>1100 Wien</p>
                        <p>+436704025953</p>
                        <p>nachhilfe.shamoon@gmail.com</p>
                    </div>

                    <div class="bewertung">
                        <p style="font-weight: bold">IHRE MEINUNG ZÄHLT!</p>
                        <p>Bitte bewerten Sie uns hier:</p>
                        <div class="bewertung_sterne">
                            <img style="width:150px; " src="img/sterne.png">
                            <div class="bewertenButton">Jetzt bewerten</div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="unterfooter">
                <div class="unterfooterInhalt">
                    <span >2021 |</span> <span class="unterfooterMenuepunkt impressum">Impressum</span> <span>|</span> <span class="unterfooterMenuepunkt datenschutz">Datenschutz </span> <span>|</span> <span class="unterfooterMenuepunkt kontakt">Kontakt</span>
                </div>
            </div>
        </footer>
    </body>
</html>
