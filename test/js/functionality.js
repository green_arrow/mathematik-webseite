/* main functionallity, available after the first page load */
jQuery(document).ready(function($){

    /* click handler for left and top navigation */
    $(".mdl-navigation").click(function(event){
        event.preventDefault();
        let navigationLinkTarget = event.target.getAttribute('data-site-to-load');
        console.log('load link : ' + navigationLinkTarget);
        load(navigationLinkTarget);

        let isSidenavClicked = event.target.getAttribute('data-sidenav');

        /* if the click came from left navigation */
        if (isSidenavClicked === "true") {
            $(".mdl-layout__drawer").toggleClass("is-visible");
            $(".mdl-layout__obfuscator").toggleClass("is-visible");
        }

        /* remove old highlighted menus */
        $(".mdl-navigation__link").removeClass("mathematik-navigation-active");
        /* set correct highlight for menu */
        $(event.target).addClass('mathematik-navigation-active');

    });

    /* click handler for footer */
    $(".mathematik-footer").click(function(event){
        event.preventDefault();
        let navigationLinkTarget = event.target.getAttribute('data-site-to-load');
        console.log('load link : ' + navigationLinkTarget);
        load(navigationLinkTarget);

        /* remove old highlighted menus */
        $(".mdl-navigation__link").removeClass("mathematik-navigation-active");
    });

    /* apply single page application by reloaing only parts of running website */
    function load(site){
        console.log('site to load : ' + site);
        let innerHTMLAsText = '<object type="text/html" data="html/XXX.html" style="width: 100%; height: 100%"></object>';
        let innerHTML = innerHTMLAsText.replace("XXX", site);

        console.log('text = ' + innerHTML);
        document.getElementById("contentJoman").innerHTML = innerHTML;
    }
});