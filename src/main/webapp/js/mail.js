/* send contact mail */
jQuery(document).ready(function($){

    $("#sendMail").click(function(){
        console.log('button clicked');

        var name = $('#Name').val();
        var mail = $('#Email').val();
        var nachricht = $('#Nachricht').val();

        $.ajax({
            type : 'POST',
            url : '../php/sendMail.php',
            //dataType : 'json',
            data : {"send" : name, mail, nachricht}
        }).done(function(){
            console.log('authentication succeeded');
        }).fail(function(jqXHR, textStatus, errorThrown){
            console.log('authentication failed (' + jqXHR.status + '): ' + errorThrown);
        });
    });

});